import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree, Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanLoad, CanActivate {

  constructor(private _authServices: AuthService,
              private _router: Router){
    
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean {
      return this._authServices.verificaAutentificacion()
        .pipe(
          tap( estaAutenticado => {
            if( !estaAutenticado){
              this._router.navigate(['./auth/login'])
            }
          })
        );
      if( this._authServices.auth.id){
        console.log('candload', true);
        return true;
      }
      console.log('candload', false);
      console.log("Bloqueado por el authGuard - canActivate");
    return false;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean > | boolean {
      return this._authServices.verificaAutentificacion()
        .pipe(
          tap( estaAutenticado => {
            if( !estaAutenticado){
              this._router.navigate(['./auth/login'])
            }
          })
        );
      
     /*  console.log(route);
      console.log(segments); */
      if( this._authServices.auth.id){
        console.log('candload', true);
        return true;
      }
      console.log('candload', false);
      console.log("Bloqueado por el authGuard - CanLoad");
    return false;
  }
}
