import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Auth } from '../interfaces/auth.interface';
import { tap, Observable, of, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _baseUrl: string = environment.baseUrl
  private _auth: Auth | undefined;

  get auth():Auth{
    return { ...this._auth! }
  }

  constructor( private _http:HttpClient) { }

  login(){
    return this._http.get<Auth>(`${this._baseUrl}/usuarios/1`)
      .pipe(
        /* tap(resp => {
          console.log('AUtHsERVICE', resp)
          this._auth = resp
        }) */
        tap( resp => this._auth = resp),
        tap( resp => localStorage.setItem('token',resp.id))
      )
  }

  logout(){
    this._auth = undefined;
  }

  verificaAutentificacion(): Observable<boolean>{
    if( !localStorage.getItem('token')){
      return of(false);
    }

    return this._http.get<Auth>(`${this._baseUrl}/usuarios/1`)
      .pipe(
        map( auth => {
          console.log('map', auth);
          this._auth = auth
          return true;
        })
      )
  }
}
