import { Component, OnInit } from '@angular/core';
import { Heroe, Publisher } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from "rxjs/operators";
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmarComponent } from '../../components/confirmar/confirmar.component';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [`
    img{
      width: 100%;
      border-radius: 5px;
    }
  `
  ]
})
export class AgregarComponent implements OnInit {
  publishers = [
    {
      id: 'Dc Comics',
      desc: 'DC - COMICS'
    },
    {
      id: 'Marvel Comics',
      desc: 'MARVEL - COMICS'
    }

  ]

  heroe: Heroe ={
    superhero:'',
    alter_ego:'',
    characters:'',
    first_appearance:'',
    publisher: Publisher.DCComics,
    alt_img:'',
  }

  constructor( private serviceHeroe: HeroesService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private _snakbar: MatSnackBar,
              private _dialog: MatDialog) { }

  ngOnInit(): void {
    if(!this.router.url.includes('editar')){
      return;
    }
    this.activatedRoute.params
    .pipe(
      switchMap( ({id})=> this.serviceHeroe.getHeroePorId(id))
    )
    //.subscribe( ({id }) => console.log(id))
    .subscribe( heroe => this.heroe = heroe);

  }

  guardar(){
    if(this.heroe.superhero.trim().length === 0){
      return;
    }

    if(this.heroe.id){
      //actualiza
      this.serviceHeroe.acatualizarHeroe( this.heroe)
      .subscribe(heroe =>{
         console.log('Actualizar', heroe)
         this.mostratSnakbar('Registro actualizado')
        })
    }else{
      //editar
      this.serviceHeroe.agreagarHeroe(this.heroe)
      .subscribe( respHeroe => {
        console.log('Respuesta', respHeroe);
        //cambio de ruta una vez agragado un nuevo personaje
        this.router.navigate(['/heroes/editar', respHeroe.id])
        this.mostratSnakbar('Registro creado')
      })
    }
    console.log("heros", this.heroe);
   /*   */
  }

  borrar(){
   const dialog =  this._dialog.open(ConfirmarComponent,{
      width: '250px',
      //data: { ...this.heroe}
      data: this.heroe
    });

    dialog.afterClosed().subscribe(
      (result) => {
        console.log("dialog", result);
        if(result){
          this.serviceHeroe.eliminarHeroe(this.heroe.id!)
          .subscribe( resp => {
            this.router.navigate(['/heroes/listado'])
          }); 
        }
      }
    )
  /*   
    */
  }

  mostratSnakbar(mensaje: string){
    this._snakbar.open(mensaje, 'Ok!',{
      duration:2500
    })
  }
}
